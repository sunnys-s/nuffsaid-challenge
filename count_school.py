import csv
import itertools
import time
import re

class School:
    ncessch = ""
    lea_id = ""
    lea_name = ""
    school_name = ""
    city = ""
    state = ""
    lat = ""
    log = ""
    m_locale = ""
    u_locale = ""
    status = ""

    def __init__(self, ncessch, lea_id, lea_name, school_name, city, state, lat, log, m_locale, u_locale, status):
        self.ncessch = ncessch
        self.lea_id = lea_id
        self.lea_name = lea_name
        self.school_name = school_name
        self.city = city
        self.state = state
        self.lat = lat
        self.log = log
        self.m_locale = str(m_locale)
        self.u_locale = str(u_locale)
        self.status = status

class Schools:
    school_list = []
    def __init__(self):
        with open('school_data.csv', 'r', encoding='cp1252') as f:
            reader = csv.reader(f)
            next(reader)
            for row in reader:
                self.school_list.append(School(row[0],row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10]))

    def count_total(self):
        return len(self.school_list)

    def group_by_column_iterator(self, column_name):
        results = []
        iterator = itertools.groupby(sorted(self.school_list, key= (lambda y: getattr(y, column_name))), key=lambda x : getattr(x, column_name) )
        for key, group in iterator:
            res = {}
            res[column_name] = key
            res['count'] = len(list(group))
            results.append(res)
        return results

    def print_group_by_column(self, label, column_name):
        print("\nSchools by %s" %(label))
        grouped = self.group_by_column_iterator(column_name)
        for res in grouped:
            print('%s: %d' %(res[column_name], res['count']))

    def print_city_with_max_school(self, column_name):
        results = self.group_by_column_iterator(column_name)
        result = max(results, key=lambda x: x['count'])
        print('\nCity with most schools: %s (%d schools)' %(result[column_name], result['count']))

    def uniq_cities(self, column_name):
        results = self.group_by_column_iterator(column_name)
        print('\nUnique cities with at least one school: %d' % len(results))

    def search(self, params):
        start_time = time.time()
        params_array = [' '.join(l) for l in list(itertools.permutations(params.split(' '),len(params.split(' ')) ))]
        params_array = params_array + [' '.join(l) for l in list(itertools.permutations(params.split(' '),len(params.split(' ')) - 1))]
        results = [school for school in self.school_list if params.upper() in (school.school_name.upper() + " " + school.city.upper())]
        if len(results) <= 0:
            for p in params_array:
                res = [school for school in self.school_list if p.upper() in (school.school_name.upper() + " " + school.city.upper())]
                results = results + res
        end_time = time.time()
        return results, round((end_time- start_time), 5)

    def print_counts(self):
        print("Total Schools : %d" % self.count_total())
        self.print_group_by_column(label='state', column_name='state')
        self.print_group_by_column(label='Metro-centric locale', column_name='m_locale')
        self.print_city_with_max_school(column_name='city')
        self.uniq_cities(column_name='city')

if __name__ == '__main__':
    count_school = Schools()
    count_school.print_counts()
