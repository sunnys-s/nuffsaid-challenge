import csv
import time

class Schools():
	def __init__(self):
		self.school_list = dict()
		self.current_state = ''
		self.current_city = ''
		self.results = list()
		self.score_location = 0
		self.score_school = 0
		
		state = 5
		city = 4
		school_name = 3

		with open('school_data.csv', 'r', encoding='cp1252') as csv_file:
			csv_reader = csv.reader(csv_file,delimiter=',')
			for row in csv_reader:
				if row[0] == 'NCESSCH':
					continue
				if row[state] not in self.school_list:
					self.school_list[row[state]] = dict()
				if row[city] not in self.school_list[row[state]]:
					self.school_list[row[state]][row[city]]= list()
				self.school_list[row[state]][row[city]].append(row[school_name])


	def search_schools(self,query):
		start_ms = time.time()
		def extract(node,search_string):
			if isinstance(node,dict):
				for k,v in node.items():
					if len(k) == 2:
						self.current_state = k
					else:
						self.current_city = k
					self.score_location = 0
					for i in [x.strip().upper() for x in search_string.split(' ')]:
						if i in k:
							self.score_location+=1
					if isinstance(node,dict):
						extract(v,search_string)
			elif isinstance(node,list):
				self.score_school = 0
				for school_name in node:
					for i in [x.strip().upper() for x in search_string.split(' ')]:
						if i == 'SCHOOL':
							continue
						if i in school_name and i not in self.current_city and i not in self.current_state:
							self.score_school+=5
						else:
							self.score_school-=1
					if self.score_school > 0 or self.score_location > 0:
						total_score = self.score_school + self.score_location
						self.results.append((total_score,'{} => {}, {}'.format(school_name,self.current_city,self.current_state)))
					self.score_school = 0

		extract(self.school_list,query)
		stop_ms = time.time()
		print('Results for "{}" (search took: {}s)'.format(query,round(stop_ms - start_ms, 4)))
		results = sorted(self.results)[-2:]
		print('results:\n{}'.format('\n'.join([x[1] for x in results])))
		self.results = list()

if __name__ == '__main__':
	school_search = Schools()
	school_search.search_schools('elementary school highland park')
	school_search.search_schools('jefferson belleville')
	school_search.search_schools('riverside school 44')
	school_search.search_schools('granada charter school')
	school_search.search_schools('foley high alabama')
	school_search.search_schools('KUSKOKWIM')
